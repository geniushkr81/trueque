
const routes = [
  {
    
      path: '/',
      component: () => import('layouts/Users.vue'),
      children: [
        {
          path: '/login/',
          name: 'login',
          component: () => import('pages/Login.vue')
        },
        {
          path: '/register/',
          name: 'register',
          component: () => import('pages/Register.vue')
        },
        {
          path: '/intro/',
          name: 'intro',
          component: () => import('pages/Intro.vue')
        },
        {
          path: '/contract/',
          name: 'contract',
          component: () => import('pages/Contract.vue')
        },
        {
          path: '/tabs/',
          name: 'tabs',
          component: () => import('pages/MenuTabs.vue')
        },
      ]
    },
    {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/main/', component: () => import('pages/Index.vue') },
      {
        path: '/auth',
        component: () => import('pages/Login.vue')
      },
      {
        path: '/directory/',
        name: 'directory',
        component: () => import('pages/Directory.vue')
      },
      {
        path: '/transfer/',
        name: 'transfer',
        component: () => import('pages/Transfer.vue')
      },
      {
        path: '/balance/',
        name: 'balance',
        component: () => import('pages/Balance.vue')
      }

    ]
  },
  

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
